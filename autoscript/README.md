Simple and dirty script to automate converting MIDI to code and uploading the code to Arduino. Very likely to break.

Requirements:

- [`arduino-cli` tool](https://github.com/arduino/arduino-cli)
- the converter program *compiled* in `../converter/main.jar`
  * compile using: `cd ../converter; kotlinc src/main/kotlin/Main.kt -include-runtime -d main.jar`
- the arduino program in `../arduino/musical-floppy/musical-floppy.ino`

## arduino-cli setup

See `arduino-cli` docs for how to get started. With Arduino UNO I did this:
```
arduino-cli core update-index
```
```
arduino-cli board list
```
output:
```
Port         Protocol Type              Board Name  FQBN            Core       
/dev/ttyACM0 serial   Serial Port (USB) Arduino Uno arduino:avr:uno arduino:avr
/dev/ttyS0   serial   Serial Port       Unknown
```

Core string for my board is `arduino:avr`, so I install it by running
```
arduino-cli core install arduino:avr
```

And now I can compile the sketch.
