#!/bin/bash

# Converts midi, into commands for arduino, inserts them into sketch, compiles it and uploads it to arduino
#
# Assumes there is converter compiled in ../converter/main.jar and the sketch in ../arduino/musical-floppy/musical-floppy.ino
#
# Also requires arduino-cli
#
# USAGE: ./convert-compile-upload.sh <MIDI file> <note shift down>
#
# See comments in ../converter/src/main/kotlin/Main.kt for more details or run "java -jar ../converter/main.jar -h"

ARDUINO_PORT='/dev/ttyACM0'
MIDI_FILE=$1
DOWN=$2
[ -z "$DOWN" ] && DOWN=0

cp ../converter/main.jar main.jar
mkdir -p sketch
java -jar main.jar "$MIDI_FILE" ../arduino/musical-floppy/musical-floppy.ino --shiftDown "$DOWN" -f 3 > sketch/sketch.ino
rm main.jar

arduino-cli compile --fqbn arduino:avr:uno sketch
arduino-cli upload -p $ARDUINO_PORT --fqbn arduino:avr:uno sketch
