#define CNT 3

//Program for arduino to play music on FDDs

//step pins - FDD makes a step on rising edge of HIGH.
int stepPins[CNT] = {2,4,6};
//direction pins - LOW is towards the center of FDD.
int dirPins[CNT] = {3,5,7};

//current direction of FDD
int dirs[CNT] = {1,1,1};
//whether step pin is HIGH or LOW
int parts[CNT] = {0,0,0};

//offset added to the note time. Set to the current millis on song start.
long startTime = 0;

void setup() {
  Serial.begin(9600);
  for(int i = 0; i < CNT; i++){
    pinMode(stepPins[i], OUTPUT);
    pinMode(dirPins[i], OUTPUT);
  }

  reset();
}

void writeAllStep(int value){
  for(int i = 0; i < CNT; i++){
    digitalWrite(stepPins[i], value);
  }
}
void writeAllDir(int value){
  for(int i = 0; i < CNT; i++){
    digitalWrite(dirPins[i], value);
  }
}

//ensure all stepper motors are free to move
void reset(){
  writeAllDir(HIGH);
  for(int i = 0; i < 80; i++){
    writeAllStep(LOW);
    delay(20);
    writeAllStep(HIGH);
    delay(20);
  }
  writeAllDir(LOW);
  for(int i = 0; i < 5; i++){
    writeAllStep(LOW);
    delay(20);
    writeAllStep(HIGH);
    delay(20);
  }
}

// Plays given periods on the FDDs while the current time is lower than until+startTime. Period 0 means silence.
void play(long until, unsigned int periods[]){
  until = startTime + until;
  //Serial.println("until: " + String(until) + " time: " + millis() + " notes: " + String(periods[0]) + ", " + String(periods[1]));
  //When state of step pins needs changing on each drive
  long driveUntils[CNT];
  long now = micros();
  //calculates driveUntil
  for (int i = 0; i < CNT; i++){
    if(periods[i] == 0){
      driveUntils[i] = 2147483640;
      periods[i] = 65350;
    }else{
      //periods are divided in half because on the beginning of each period step pin is set to HIGH *click* and in the half it is set back to LOW (no click)
      driveUntils[i] = now + periods[i]/2;
    }
  }
  long smallestUntil = until*1000;
  for(int i = 0; i < CNT; i++){
    if(driveUntils[i] < smallestUntil){
      smallestUntil = driveUntils[i];
    }
  }
  //until when to play curretn periods
  while (now < until*1000){
    //if something needs switching
    if(now >= smallestUntil){
      for(int i = 0; i < CNT; i++){
        if(now>driveUntils[i]){
          //whis FDD needs switching
          //add half period to driveUntil
          driveUntils[i] = driveUntils[i] + periods[i]/2;
          //all this is to correctly switch step pin voltage every half period and to alternate direction every step.
          if(parts[i] == 0){
            digitalWrite(stepPins[i], HIGH);
            parts[i] = 1;
          }else{
            digitalWrite(stepPins[i], LOW);
            if(dirs[i] == 1){
              digitalWrite(dirPins[i], HIGH);
            }else{
              digitalWrite(dirPins[i], LOW);
            }
            dirs[i] = dirs[i] * -1;
            parts[i] = 0;
          }
        }
      }
      //find the closest time something needs switching
      smallestUntil = until*1000;
      for(int i = 0; i < CNT; i++){
        if(driveUntils[i] < smallestUntil){
          smallestUntil = driveUntils[i];
        }
      }
    }
    now = micros();
  }
}

//note periods numbered by their MIDI number
const unsigned int notePeriods[128] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  61162, 57737, 54496, 51414, 48544, 45809, 43253, 40816, 38521, 36364, 34317, 32394, //C0 - B0
  30581, 28860, 27241, 25714, 24272, 22910, 21622, 20408, 19264, 18182, 17161, 16197, //C1 - B1
  15288, 14430, 13620, 12857, 12134, 11453, 10811, 10204, 9631, 9091, 8581, 8099, //C2 - B2
  7645, 7216, 6811, 6428, 6068, 5727, 5405, 5102, 4816, 4545, 4290, 4050, //C3 - B3
  3822, 3608, 3405, 3214, 3034, 2863, 2703, 2551, 2408, 2273, 2145, 2025, //C4 - B4
  1911, 1804, 1703, 1607, 1517, 1432, 1351, 1276, 1204, 1136, 1073, 1012, //C5 - B5
  956, 902, 851, 804, 758, 716, 676, 638, 602, 568, 536, 506, //C6 - B6
  478, 451, 426, 402, 379, 358, 338, 319, 301, 284, 268, 253, //C7 - B7
  239, 225, 213, 201, 190, 179, 169, 159, 150, 142, 134, 127,//C8 - B8
  0, 0, 0, 0, 0, 0, 0, 0
};

//shortcut
void playNotes(long until, int notes[]){
  unsigned int periods[CNT];
  for(int i = 0; i < CNT;i++){
    periods[i] = notePeriods[notes[i]]/2; //it is divided by two otherwise it would be one octave off.
  }
  play(until,periods);
}

//shortcut
void playThreeNotes(long until, int note1, int note2, int note3){
  int notes[3] = {note1, note2,note3};
  playNotes(until, notes);
}

//shortcut
void p(long until, int note1, int note2, int note3){
  playThreeNotes(until, note1, note2, note3);
}

//main loop - /* {{MUSIC--DATA}} */ (with one dash only) will be replaced by calls of p() with song data
void loop() {
    startTime = millis();
/* {{MUSIC-DATA}} */

}
