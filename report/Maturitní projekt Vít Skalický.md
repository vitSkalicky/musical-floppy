# Maturitní projekt: Hudební disketové mechaniky

Vít Skalický <[vit.skalicky@email.cz](mailto:vit.skalicky@email.cz)>, 30. dubna 2022

## Úvod

Pokud si pamatujete doby, kdy se místo flash disků a CD disků používaly
diskety, jistě víte, že zařízení pro čtení disket - disketové
mechaniky - jsou trochu hlučné. Zvuk, který vydávají, mění při čtení jak
svoji hlasitost tak i tón. A za pomoci správného zapojení a chytrého
programu je možné tento zvuk ovládnout a zahrát jednoduché melodie nebo
i celé skladby. A právě zahrát několik písní jen pomocí disketových
mechanik (dále jen z anglického *floppy disk drive*) je můj cíl pro
tento maturitní projekt.

V případě že nemáte tušení co je disketa a nevíte jak disketová
mechanika vypadá, tak následuje stručný popis. Disketa se skládá z
magnetického kotouče, který je chráněn plastovým krytem. Na kotouč se
zapisují a čtou data pomocí disketové mechaniky - zařízení, které plní
podobnou funkci jako CD mechanika, akorát pro diskety. Přibližně do roku
2000 byly disketové mechaniky standardní výbavou téměř všech počítačů,
poté však byly jak diskety tak disketové mechaniky postupně nahrazeny CD
a CD mechanikami a ty později ještě menšími Flash disky a cloudovým
úložištěm. Samotné disketové mechaniky čtou a zapisují data na disketu
pomocí čtecí/zapisovací hlavy, která se pohybuje od středu k okraji
magnetického kotouče. Pro pohyb hlavy je použit krokový motorek, který
při práci vydává bzučivý zvuk. A právě zvuk disketových mechanik se stal
synonymem pro počítače osmdesátých a devadesátých let.

![Disketová mechanika](img/FDD.png.jpg){width="100%"}

Samozřejmě nejsem první kdo se pokusil na FDD zahrát hudbu. O tom, že je
něco takového možné, jsem se dozvěděl, když jsem shlédl video od
polského kuťila jménem Paweł Zadrożniak nazvané . Floppotron se skládá z
64 FDD, 8 pevných disků a 2 tiskáren. Díky tomu dokáže zahrát téměř
jakoukoliv skladbu. Já se nesnažím o nic ani zdaleka tak velkého jako
Pawlův Floppotron - podařilo se mi sehnat pouhé 3 FDD a s tím si musím
vystačit.

![Zařízení jménem Floppotron které sestavil Paweł Zadrożniak.
<http://floppotron.com/> ](img/floppotron.png.jpg){width="100%"}

## Cíl práce

Cílem mé práce je postavit a naprogramovat zařízení, které bude pomocí
pouze 3 disketových mechanik hrát hudbu.

## Postup

Nejdříve bych rád vysvětlil samotný princip fungování tohoto projektu.

Každá disketová mechanika obsahuje krokový motor (anglicky *stepper
motor*). Krokový motor má stejně jako každý jiný elektromotor nějaké
cívky a magnety. Počet cívek a jejich zapojení závisí na konkrétním
druhu motoru. Krokový motor je však zkonstruován tak, že pokaždé když
částí cívek protéká proud, rotor se pootočí o daný úhel. Poté je třeba
přesměrovat proud do jiné sady cívek a rotor udělá další přesně vyměřený
krok, a tak dále. Díky tomu můžeme velmi přesně ovládat úhel, o který se
rotor motoru pootočí, což je potřeba právě v FDD, kde krokový motor musí
přesně umístit čtecí hlavu na správný sektor diskety.

Malé nebo levnější krokové motory však při každém kroku vydají
slyšitelné cvaknutí. Pokud chceme aby se motor otáčet rychleji, musíme
dělat kroky také rychleji a motor bude vydávat více cvaknutí za vteřinu.
Pokud budeme s motorem dělat přesně 220 kroků za vteřinu, lidskému uchu
cvakání splyne v bzučivé A~3~.

Pro zahrání hudby je tedy potřeba provádět velmi rychlé a přesně
načasované kroky motorkem v FDD.

Jak si můžete všimnout, motorek nestačí pouze připojit na zdroj, ale je
třeba ve správných intervalech proud do různých částí motorku vypínat a
zapínat. K tomu se dá použít mikrokontroler jménem Arduino. Jedná se
malý o procesor na desce ke které lze snadno připojit různé LED diody,
tlačítka, senzory či motorky. Do procesoru je poté možné nahrát program
kterým můžeme připojené součástky ovládat. Já Arduino využiji k přesně
načasovanému ovládání motorku v FDD.

Arduino je schopné spouštět pouze velmi jednoduché programy, proto bude
nutné vytvořit ještě další program, který zpracuje složité písničky do
posloupnosti frekvencí a doby po kterou mají hrát, protože to je jediné
čemu můj program na Arduinu rozumí.

Projekt jsem tedy rozdělil do 3 částí:

-   Hardware - zapojení FDD, napájení, připojení k Arduinu.

-   Program Arduina - program, který bude řídit samotný hardware a podle
    dat skladby ovládat motorky.

-   Překladač z MIDI - program, kterým dokáže zpracovat MIDI soubor
    (standard pro zapisování hudby ve formě not) a přeložit ho do
    jednoduchého formátu, kterému porozumí program na Arduinu.

### Hardware

#### Zdroj

Nejdříve je potřeba veškerou elektroniku napájet. K tomu využívám zdroj
ze starého počítače. Většina zdrojů, včetně toho mého, má však pouze
jeden konektor pro FDD, proto jsem musel odstranit kryt jednoho
přebytečného konektoru typu Molex, který má stejné dráty jako konektor
pro FDD (12V DC, zem, zem, 5V DC), ale špatný tvar, a připájel jsem k
obnaženým drátům drátky pro nepájivá kontaktní pole, pomocí kterých jsem
připojil zbylé dvě FDD. Žlutý drát je 12V DC, červený 5V DC a černé jsou
zem.

![Napájecí konektor pro FDD - zdroj má pouze
1](img/FDD_Connector.png.jpg){width="100%"}

![Konektor typu MOLEX](img/MOLEX.png.jpg){width="100%"}

![Dráty z nepájivého kontaktního pole připájené místo MOLEX
konektoru)](img/MOLEX_soldered.png.jpg){width="100%"}

Po připojení do zásuvky však zdroj nebude nic napájet, protože je
potřeba jej nejdříve zapnout. Jelikož není připojen k počítači, je
potřeba spojit piny číslo 16 a 17 na ATX konektoru, což lze udělat
například ohnutým kusem měděného drátu.

![Zapnutí zdroje propojením pinů č. 16 a
17](img/ATX_shorted.png.jpg){width="50%"}

#### Disketové mechaniky

Ovládání motorku je usnadněno tím, že FDD v sobě mají již zabudovaný
driver motorku (obvod starající se o správné napájení cívek). FDD má
zezadu 34 pinů a další 4 piny pro napájení. přičemž pin číslo 20 ovládá
kroky motorku a pin 18 ovládá směr. Pokaždé když je pin 20 uzemněn
(LOW), udělá motorek jeden krok. Když je při tom pin 18 uzemněn (LOW),
posune to čtecí hlavu směrem ke středu diskety, pokud nebude uzemněn
(HIGH), posune to čtecí hlavu k okraji diskety.

Celkové zapojení FDD tedy bude následující:

-   Napájecí piny připojíme ke zdroji.

-   Pin 20 připojíme k Arduinu (první FDD na pin 2, druhé na 4, třetí na
    6).

-   Pin 18 připojíme k Arduinu (první FDD na pin 3, druhé na 5,\...).

-   Pin 19 připojíme k zemi na Arduinu.

-   Pin 11 připojíme na pin 12, čímž zapneme v FDD ovládání motorku.

Zapojení znázorněno na obrázku [1](#fig:schem){reference-type="ref"
reference="fig:schem"} a obrázek [2](#fig:pinmums){reference-type="ref"
reference="fig:pinmums"} ukazuje jak jsou piny na FDD počítány.

![Schéma zapojení jednoho FDD k
Arduinu](img/FDD_schematic.png.jpg){#fig:schem width="70%"}

![Číslování pinů na FDD. Správnou orientaci určuje pin 3, který chybí
(ale počítá se).](img/FDD_pinout.png.jpg){#fig:pinmums width="100%"}

#### Arduino

Zapojení samotného Arduina je velmi jednoduché - na pinech 2 až 7 máme
připojena FDD a chybí nám jen napájení desky s procesorem. Při vývoji a
testování jej bude zajišťovat USB připojení k počítači a pro napájení ze
zdroje jsem rozřízl USB kabel, příslušné dráty připojil na 5V DC a zem z
rozebraného Molex konektoru od zdroje a kabel zapojil do USB portu
Arduina stejně jako předtím při napájení z PC.

### Program Arduina

Základní funkcí programu Arduina je funkce `play`, která má 2 argumenty:
seznam period not (v mikrosekundách), které má hrát na jednotlivých FDD
a číslo milisekundy (počítáno od začátku běhu programu), do které má
dané noty hrát. Její kód můžete vidět níže.

V poli `driveUntils` je uloženo ve kterou mikrosekundu se má na každém
FDD přepnout napětí na krokovém pinu (pin 20 na FDD). Pokaždé když je
aktuální čas větší než hodnota v `driveUntils`, napětí na krokovém pinu
se přepne a k hodnotě v `driveUntils` se přičte polovina periody noty.
Protože motorek v FDD udělá krok jen když se napětí na krokovém pinu
mění z LOW na HIGH, tedy jednou za 2 přepnutí, a program přepíná napětí
po polovině periody noty, tak budou motorky dělat kroky s rozestupem
jedné periody noty.

Kód funkce play:

``` {.c language="c" style="mystyle" texcl="true"}
// Hraje periody na jednotlivá FDD dokud je aktuální čas menší
// než until+startTime. Perioda 0 znamená ticho.
void play(long until, unsigned int periods[]){
  until = startTime + until;
  // Kdy končí aktuální perioda na jednotlivých FDD a bude třeba udělat
  // další krok.
  long driveUntils[CNT];
  long now = micros();
  // spočítá prvotní driveUntil
  for (int i = 0; i < CNT; i++){
    if(periods[i] == 0){
      driveUntils[i] = 2147483640;
      periods[i] = 65350;
    }else{
      // Perioda se dělí 2, protože na začátku periody se nastaví
      // krokový pin na HIGH *cvak* a v půlce zpátky na LOW
      // (žádné cvak).
      driveUntils[i] = now + periods[i]/2;
    }
  }
  long smallestUntil = until*1000;
  for(int i = 0; i < CNT; i++){
    if(driveUntils[i] < smallestUntil){
      smallestUntil = driveUntils[i];
    }
  }
  // dokud se má hrát
  while (now < until*1000){
    // když je třeba něco přepnout
    if(now >= smallestUntil){
      for(int i = 0; i < CNT; i++){
        if(now>driveUntils[i]){
          // tohle FDD je třeba přepnout
          // posuň driveUntil o další půlperiodu
          driveUntils[i] = driveUntils[i] + periods[i]/2;
          // Toto větvení zde je proto, aby se dělal vždy jeden krok
          // jedním směrem a další zase zpět a taky aby se po jedné
          // půlperiodě krokový pin nastavil na HIGH a po druhé na LOW.
          if(parts[i] == 0){
            digitalWrite(stepPins[i], HIGH);
            parts[i] = 1;
          }else{
            digitalWrite(stepPins[i], LOW);
            if(dirs[i] == 1){
              digitalWrite(dirPins[i], HIGH);
            }else{
              digitalWrite(dirPins[i], LOW);
            }
            dirs[i] = dirs[i] * -1;
            parts[i] = 0;
          }
        }
      }
      // najde se nejblišší čas kdy bude nutné něco přepnout
      smallestUntil = until*1000;
      for(int i = 0; i < CNT; i++){
        if(driveUntils[i] < smallestUntil){
          smallestUntil = driveUntils[i];
        }
      }
    }
    now = micros();
  }
}
```

Dále jsou ve statickém poli uloženy periody všech not, očíslovány stejně
jako ve formátu MIDI. Nakonec jsem si vytvořil několik pomocných funkcí
pro pohodlnější hraní not. Výsledkem je funkce
`void p(long until, int note1, int note2, int note3)`. Chci-li hrát
vteřinu na všech třech FDD notu C~2~ a poté jen na prvním D~2~, vložím
do hlavní smyčky programu následující kód:

``` {.c language="c" style="mystyle" texcl="true"}
void loop() {
  startTime = millis();
  p(1000, 36, 36, 36);
  p(2000, 38, 0, 0);
}
```

### Překladač z MIDI

Jak si můžete všimnout, program na Arduinu není schopen zpracovat
samotné MIDI, ve kterém jsou písničky uloženy, ale používá vlastní velmi
jednoduchý formát. Proto bylo také potřeba vytvořit program pro písniček
z MIDI souboru. Nejdřív je však nutné vědět, co MIDI soubor vůbec je.

#### MIDI

MIDI je standard pro digitální zapisování hudby ve formě not. Na rozdíl
od formátů jako MP3 neobsahuje samotné zvukové vlny, ale pouze noty,
stejně jako když je má klavírista napsané na papíře. Pro použití MIDI
jsem se rozhodl ze dvou důvodů. Za prvé je velmi rozšířené, díky čemuž
můžete mnohé skladby najít na internetu již přepsané do MIDI, a za druhé
existuje široká škála programů pro jeho úpravu.

Pro zahrání nějaké skladby je nejdříve potřeba sehnat MIDI soubor.
Hudebně nadaný člověk by mohl v editoru zapsat noty skladby, ale to já
nejsem, proto MIDI stáhnu někde z internetu. Značnou limitací je fakt,
že 1 FDD může vydávat pouze 1 tón najednou. Se třemi tedy zahraji
maximálně 3 noty současně. Proto je třeba MIDI upravit tak, aby se nikde
nevyskytovalo moc not najednou. Prakticky to znamená, že smí zůstat jen
hlavní melodie skladby a výjimečně i vedlejší, pokud neobsahuje žádné
akordy. Noty pro každé FDD se musí umístit do vlastní stopy pojmenované
, , atd. Z těchto stop bude překladač noty číst.

MIDI je binární soubor, který začíná hlavičkou, ve které je zapsáno
kolik obsahuje stop a jaká je rychlost skladby. Následují jednotlivé
stopy, které se skládají z posloupnosti událostí. Událost může být
například začátek či konec noty nebo , která kóduje například název
stopy, informace o nástroji nebo konec stopy.

#### Program

Program jsem napsal v jazyce Kotlin, který je založen na jazyce Java. V
Javě existuje knihovna `javax.sound.midi`, která za mě obstará čtení
MIDI souboru a umožní mi s ním pohodlněji pracovat.

``` {.kotlin language="kotlin" texcl="true"}
import javax.sound.midi.*

val sequence: Sequence = MidiSystem.getSequence(inputStream)
```

Nejdříve se vytvoří proměnná do které se budou zpracované noty každé
stopy ukládat spolu s časem od kterého mají hrát. Každá stopa může
maximálně jednu notu v jeden okamžik.

``` {.kotlin language="kotlin" texcl="true"}
val notes: Array<ArrayList<Pair<Int, Int>> = Array()
```

Následující kód poté projde všechny stopy a v každé všechny události. K
tomu si pro každou stopu vytvoří proměnou, do které se později uloží pro
který FDD disk je daná stopa určena.

``` {.kotlin language="kotlin" texcl="true"}
for (track in sequence.tracks) {
    var fddnumber = -1
    for (i in 0 until track.size()) {
        val event = track[i]
        // dalsí zpracování
    }
    //...
}
```

Zpracování každé události vypadá následovně. V kódu jsou vloženy
komentáře vysvětlující jeho funkci.

``` {.kotlin language="kotlin" texcl="true"}
val event = track[i]
// další zpracování 

// Konstanty pro zpřehlednění
val NOTE_ON = 0x90
val NOTE_OFF = 0x80
val TRACK_NAME = 0x03

val message = event.message

// Zkontroluje typ události.
if (message is MetaMessage){
    // Pokud to je meta událost určující název stopy, zjistí zdali má
    // tuto stopu použít pro nějaké FDD
    if (message.type == TRACK_NAME){
        // Zjistí název stopy
        val name = String(message.data)
        // Pokud název začíná na FDD
        if (name.startsWith("FDD")){
            // Ustřihne z názvu první 3 znaky a zbytek se pokusí
            // převést na číslo.
            val numStr = name.substring(3)
            val num: Int? = numStr.toIntOrNull()
            if (num != null){
                // Od čísla odečte 1, protože názvy stop začínají 
                // na FDD1, ale tento program čísluje stopy od 0.
                fddnumber = num - 1
                // V proměnné fddnumber je nyní uloženo číslo FDD
                // pro které je stopa určena a program tedy může
                // začít se zpracováváním not.
            }
        }
    }else {
        // Ostatní meta události - ignorováno
    }
}else 
// Normální události. Pokud stopa není určena pro žádné FDD, jsou
// ignorovány.
if (message is ShortMessage && fddnumber > -1) {
    // Začátek noty
    if (message.command == NOTE_ON) {
        // Číslo noty - např. 24 je C1
        val key = message.data1
        // Uloží notu a čas.
        notes[fddnumber].add(Pair(event.tick.toInt(), key))
    }
    // Konec noty
    else if (message.command == NOTE_OFF) {
        // Opět číslo noty která má být ukončena
        val key = message.data1
        // Vzhledem k tomu, že tento program narozdíl od MIDI ukládá
        // pouze jednu notu pro jednu stopu v jeden ukamžik, tak musí
        // zkontrolovat že událost ukončení noty skutečně ukončuje
        // poslední uloženou notu a ne nějakou jinou, která již byla
        // přepsána začátkem jiné noty.
        if (key == notes[fddnumber].last().second) {
            notes[fddnumber].add(Pair(event.tick.toInt(), 0))
        }
    } else {
        // Ostatní normální události - ignorováno
    }
}else{
    // Ostatní události - ignorováno
}
```

Program výše zpracuje MIDI soubor a do seznamu `notes` uloží pro každé
FDD seznam dvojic čísel určujících čas a jaká nota se v daný čas má
začít hrát. 0 značí žádnou notu. Můžete si však všimnout, že jako čas se
neukládají milivteřiny, ale `event.tick`. MIDI totiž pro časování
událostí používá ticky, které mají různou délku podle tempa skladby.
Naštěstí není složité převést ticky na milivteřiny.

``` {.kotlin language="kotlin" texcl="true"}
val microsPer1000Ticks: Long =
  (sequence.microsecondLength*1000L) / sequence.tickLength

notes.forEach { tracknotes ->
  for (i in 0 until tracknotes.size){
    tracknotes[i] = Pair(
      ((tracknotes[i].first * microsPer1000Ticks)/1000000)
        .toInt(),
      tracknotes[i].second
    )
  }
}
```

Tento formát je již velmi blízko tomu, co Arduino dokáže zpracovat.
Pomocí několika dalších jednodušších funkcí, které zde již nebudu
rozebírat, program vytvoří kód, který se vloží do hlavní smyčky programu
pro Arduino.

``` {.c language="c" style="mystyle" texcl="true"}
p(3278, 0, 0, 0);
p(3442, 0, 64, 0);
p(3606, 0, 66, 0);
p(3770, 0, 67, 0);
p(3934, 0, 66, 0);
p(4098, 0, 67, 0);
p(4262, 0, 69, 0);
//...
```

## Výsledek

Konečné zařízení se skládá ze 3 disketových mechanik, Arduina a zdroje,
který vše napájí. Díly jsou propojeny standardními kabely ze zdroje a
také pomocí nepájivých kontaktních polí. Po zapojení zdroje do sítě
začne zařízení hrát hudbu, která je právě nahrána na Arduinu.

Pro nahrání hudby do zařízení je nejdříve třeba sehnat MIDI soubor, ten
upravit, aby se nikde nevyskytovaly více než 3 noty současně, pomocí
prvního programu jej přeložit do kódu pro Arduino, který se vloží do
druhého programu. Druhý program se poté musí zkompilovat a nahrát na
samotné Arduino které se pomocí USB připojí k počítači. Jakmile je
program nahraný, odpojí se Arduino od počítače a zapojí se do napájení
ze zdroje. Po zapnutí zdroje začne zařízení hrát.

Zařízení má bzučivý elektronický zvuk. Dokáže zahrát tóny od přibližně
A~0~ až do A~3~, kvůli čemuž je často nutné skladby posunout o několik
tónin níže. Největší limitací je skutečnost, že s pouhými 3 FDD není
možné zahrát více než 3 noty současně. Všechny skladby je tedy nutné
omezit pouze na hlavní melodii a výjimečně i vedlejší. Akordy lze hrát
pouze ty nejjednodušší skládající se ze 3 not.

## Závěr

Podařilo se mi sestavit zařízení schopné zahrát hudbu pomocí disketových
mechanik.

Zpracovávání hudby do použitelného formátu se nakonec ukázalo být
složitější než jsem čekal. Kdybych stejný projekt dělal znovu, použil
bych knihovnu pro Arduino, která mu umožňuje chovat se jako MIDI
zařízení. Díky tomu by bylo možné zařízení připojit například k
elektrickým klávesám a hrát přímo na ně. Na druhou stranu můj přístup
umožňuje snazší používání zařízení bez toho aby bylo připojené k PC.

Při tvorbě tohoto projektu jsem se naučil něco o funkci krokových
motorů, programovat složitější programy pro Arduino, strukturu MIDI
souboru a jak s ním pracovat a nakonec také něco málo o zvuku a hudbě.

![Celé zařízení (na obrázku zatím jen s 2
FDD)](img/Complete.png.jpg){width="100%"}

## Zdrojový kód

Veškerý zdrojový kód naleznete na:

<https://gitlab.com/vitSkalicky/musical-floppy>
