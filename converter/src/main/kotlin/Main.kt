import java.io.File
import java.io.InputStream
import java.lang.Integer.max
import javax.sound.midi.MetaMessage
import javax.sound.midi.MidiSystem
import javax.sound.midi.Sequence
import javax.sound.midi.ShortMessage
import kotlin.RuntimeException
import kotlin.math.min
import kotlin.system.exitProcess

const val HELP = """

USAGE: java -jar thisfile.jar [MIDI file] [template file] [OPTIONS...]

Reads the MIDI file, extracts notes from tracks named FDD1, FDD2,... and converts them into code used by the arduino
sketch. Every occurrence of "/* {{MUSIC-DATA}} */" (includes comments, but not the quotation marks) in the template file
will be replaced with the music code and complete result will be printed to standard output.

Music code format:

p(3840, 0, 0, 0);
p(4032, 0, 52, 0);
p(4224, 0, 54, 0);
p(4416, 0, 55, 0);
p(4608, 0, 54, 0);
p(4800, 0, 55, 0);
p(4992, 0, 57, 0);
p(5184, 0, 59, 0);
p(6144, 0, 0, 0);
p(6336, 0, 52, 0);

First column is tick until which the state on the row is valid and each element on the row is a note code (MIDI note
code) of the note to be played by each FDD. 0 stands for no note.

OPTIONS:
  --shiftDown n       Shift down all notes by n tones. 12 is an octave. Default is 0.
  -f n, --fddCount n  Sets the number of FDDs to n. Default is 3.
  -h, --help          Prints this text
"""

/** See [HELP]
 */

const val REPLACE_TEMPLATE = "/* {{MUSIC-DATA}} */"

fun main(args: Array<String>) {
    val err = System.err

    if (args.contains("-h") || args.contains("--help")){
        println(HELP)
        exitProcess(0)
    }

    if (args.size < 2){
        err.println("Too few arguments. run with -h for help.")
        exitProcess(1)
    }

    val midiFileName = args[0]
    val midiFile = File(midiFileName)
    if (!midiFile.isFile){
        err.print("File '${midiFile.absolutePath}' not found. Run with -h for help.")
        exitProcess(2)
    }

    val codeTemplateFileName = args[1]
    val codeTemplateFile = File(codeTemplateFileName)
    if (!codeTemplateFile.isFile){
        err.print("File '${codeTemplateFile.absolutePath}' not found. Run with -h for help.")
        exitProcess(2)
    }

    val shiftDown = getOption(args, "--shiftDown")?.toIntOrNull() ?: 0
    val fddCount = getOption(args, "--fddCount")?.toIntOrNull() ?: getOption(args, "-f")?.toIntOrNull() ?: 3

    var converted = ""
    try {
        converted = midiToCode(midiFile.inputStream(), shiftDown, fddCount)
    }catch (e: RuntimeException){
        err.println("Error parsing MIDI file '${midiFile.absolutePath}'. Is it really a valid MIDI file?")
        exitProcess(3)
    }

    codeTemplateFile.forEachLine {
        println(it.replace(REPLACE_TEMPLATE, converted))
    }
}

/**
 * Reads a midi file from [input], extracts notes on tracks named `FDD1`, `FDD2`,... and returns a code for use in arduino sketch.
 *
 * Output format:
 * ```
p(3840, 0, 0, 0);
p(4032, 0, 52, 0);
p(4224, 0, 54, 0);
p(4416, 0, 55, 0);
p(4608, 0, 54, 0);
p(4800, 0, 55, 0);
p(4992, 0, 57, 0);
p(5184, 0, 59, 0);
p(6144, 0, 0, 0);
p(6336, 0, 52, 0);
 ```
 * (first number is millisecond until which the note should be played and other numbers are notes to be played on each FDD. 0 stands for no note)
 *
 * [shiftDown] shifts all notes down - use 12 to shift by an octave. Default is 0.
 *
 * [fddCount] specifies the number of floppy disk drives to use. Default is 3.
 */
fun midiToCode(input: InputStream, shiftDown: Int = 0, fddCount: Int = 0): String{
    val notes = notesFromMidi(input, fddCount)
    val events = combineFDDs(notes)
    //shift times so that they do not represent tick of the event, but until when the event is valid
    for (i in 0 until events.size-1){
        events[i][0] = events[i+1][0]
    }

    //decrease tone
    events.forEach { for (i in 1 until it.size) it[i] = max(it[i]-shiftDown, 0) }

    return events.joinToString("\n") { "p(" + it.joinToString() + ");" }
}

val NOTE_ON = 0x90
val NOTE_OFF = 0x80
val TRACK_NAME = 0x03

/**
 * Extracts notes for the FDDs from a midi file. Each FDD has its own track named "FDDx", where x is the number of the FDD starting with 1. Examples: "FDD1", "FDD2".
 *
 * If notes in one track overlap, they will overwrite each other.
 *
 * Returns an array containing ArrayLists for each FDD that contain pairs of timestamp and note code or 0 for no note.
 */
fun notesFromMidi(inputStream: InputStream, fddCount: Int): Array<ArrayList<Pair<Int,Int>>> {
    //reads MIDI file
    val sequence: Sequence = MidiSystem.getSequence(inputStream)

    //in MIDI, time is counted in ticks. Using this I can convert ticks to micros.
    val microsPer1000Ticks: Long = (sequence.microsecondLength*1000L) / sequence.tickLength

    //notes for each FDD; FDDs<notes<tick, note>>
    val notes = Array(fddCount){ArrayList<Pair<Int, Int>>()}

    for (track in sequence.tracks) {
        //number of FDD, determined from track name; remains -1 if the track should be ignored; Track names are in format FDDx where x is a number of the FDD starting with 1. examples: FDD1, FDD2
        var fddnumber = -1
        for (i in 0 until track.size()) {
            val event = track[i]
            val message = event.message
            if (event.tick > Int.MAX_VALUE.toLong()){
                //safety check
                throw RuntimeException("Tick larger than max integer value")
            }
            if (message is ShortMessage && fddnumber > -1) { // a note command; ignore it if FDD number is not determined
                if (message.command == NOTE_ON) {
                    val key = message.data1
                    notes[fddnumber].add(Pair(event.tick.toInt(), key))
                } else if (message.command == NOTE_OFF) {
                    val key = message.data1
                    //turn off note only if it matches the currently playing one. Otherwise, the note that this event was supposed to turn off is already overwritten by the last one.
                    if (key == notes[fddnumber].last().second) {
                        notes[fddnumber].add(Pair(event.tick.toInt(), 0))
                    }
                } else {
                    //some other command, ignore
                }
            } else if (message is MetaMessage){ // a meta command
                if (message.type == TRACK_NAME){
                    // determine FDD number from track name; It is in format FDDx where x is a number of the FDD starting with 1. examples: FDD1, FDD2
                    val name = String(message.data)
                    if (name.startsWith("FDD")){
                        val numStr = name.substring(3)
                        val num: Int? = numStr.toIntOrNull()
                        if (num != null){
                            fddnumber = num - 1 //Tracks are numbered from 1
                        }
                    }
                }else {
                    //other meta commands, ignore
                    //println("//${message.type.toString(16)} ${message.data.map { it.toUByte().toString(16) }.joinToString("")}")
                }
            }else{
                //other commands, ignore
            }
        }
    }
    notes.forEach {
        it.sortBy { it.first*2 + if (it.second == 0) 0 else 1 }
    }

    //convert ticks to millis
    notes.forEach { tracknotes ->
        for (i in 0 until tracknotes.size){
            tracknotes[i] = Pair(
                ((tracknotes[i].first * microsPer1000Ticks)/1000000)
                    .toInt(),
                tracknotes[i].second
            )
        }
    }

    return notes
}

fun combineFDDs(notes: Array<ArrayList<Pair<Int,Int>>>):ArrayList<IntArray>{
    val fddCount = notes.size
    val output = ArrayList<IntArray>()
    var tick = 0
    val js = IntArray(fddCount){0}
    var currentState = IntArray(fddCount + 1)
    while (true){
        currentState[0] = tick
        var nextTick = Int.MAX_VALUE
        for (i in 0 until fddCount){
            if (js[i] < notes[i].size) {
                while (notes[i][js[i]].first == tick) {
                    //curreState[0] is tick
                    currentState[i + 1] = notes[i][js[i]].second
                    js[i]++
                    if (js[i] >= notes[i].size)
                        break
                }
                if (js[i] < notes[i].size) {
                    nextTick = min(nextTick, notes[i][js[i]].first)
                }
            }else{
                currentState[i+1] = 0
            }
        }
        if (nextTick == Int.MAX_VALUE)
            break
        tick = nextTick
        output.add(currentState.clone())
    }
    return output
}

/** utility for parsing commandline arguments */
fun getOption(args: Array<String>, key: String): String?{
    val index = args.indexOfFirst { it == key }
    if (index == -1 || index == args.size -1)
        return null
    return args[index+1]
}