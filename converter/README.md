# MIDI converter

Reads the MIDI file, extracts notes from tracks named FDD1, FDD2,... and converts them into code used by the arduino
sketch. Every occurrence of "/* {{MUSIC-DATA}} */" (includes comments, but not the quotation marks) in the template file
will be replaced with the music code and complete result will be printed to standard output.

Music code format:

    p(3840, 0, 0, 0);
    p(4032, 0, 52, 0);
    p(4224, 0, 54, 0);
    p(4416, 0, 55, 0);
    p(4608, 0, 54, 0);
    p(4800, 0, 55, 0);
    p(4992, 0, 57, 0);
    p(5184, 0, 59, 0);
    p(6144, 0, 0, 0);
    p(6336, 0, 52, 0);

First column is tick until which the state on the row is valid and each element on the row is a note code (MIDI note
code) of the note to be played by each FDD. 0 stands for no note.

## Build

1. Install kotlin
2. Open terminal in this folder
3. Run `kotlinc src/main/kotlin/Main.kt -include-runtime -d main.jar`
4. Program will be compiled into `main.jar`

## Usage

Make sure you have java installed.

To convert `moldau.midi` to music code with 3 FDDs, insert it into `../arduino/musical-floppy/musical-floppy.ino` and save the output to `moldau-code.ino` run:

    java -jar main.jar moldau.midi ../arduino/musical-floppy/musical-floppy.ino -f 3 > moldau-code.ino