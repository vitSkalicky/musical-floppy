Hudební disketové mechaniky
===========================

Zde najdete můj maturitní projekt, ve kterém jsem hrál hudbu na disketové mechaniky. Všechny podrobnosti najdete v [dokumentaci k projektu](report/Maturitní projekt Vít Skalický.md) (dostupné i jako [PDF](report/Maturitní projekt Vít Skalický.pdf))

Musical Floppy drives
=====================

Here you can find my project for Maturita, in which I played music using floppy drives. All details can be found in [project documentation 🇨🇿](report/Maturitní projekt Vít Skalický.md) ([PDF 🇨🇿](report/Maturitní projekt Vít Skalický.pdf))

## Quick summary in English

Here you can find a set of programs for playing music on floppy drives. Follow instructions on [https://www.instructables.com/Arduino-Floppy-Music/](https://www.instructables.com/Arduino-Floppy-Music/)
for wiring. Connect FFD1 step pin to pin 2 on arduino, direction pin to pin 3, FFD2 step to 4, direction to 5,...

Upload the arduino sketch found in `arduino/` to your arduino to start playing music. Music data is compiled in the sketch as calls to function `void p(long until, int note1, int note2, int note3)`. Currently the program is set up to use 3 FDDs.

The generate music code from MIDI for the arduino use kotlin program in `converter/`. See its comments and help for more instructions.

Finally there is a hacky automation script which likely won't work. See `autoscript/README.md`.

BTW MIDI Editing
----------------

May be complicated.

In the end I used [Qtractor](https://qtractor.org/) (editor) with [Qsynth](https://github.com/rncbc/qsynth) (synthetizer) on Fedora. Both are in RPM repos.
